<?php

	/*
		Author; temet: gamesforvivian@yahoo.com & Araf
		Date: Aug 26 2014
		Description:
			Sql queries to set up the database. Meant to be run a single time at installation. 

	*/



$req_t_account = "CREATE TABLE t_account (
id_acc INT (11) UNSIGNED NOT NULL AUTO_INCREMENT,
acc_email VARCHAR (250) NOT NULL,
acc_pwd CHAR (32) NOT NULL,
acc_name VARCHAR (50) NOT NULL,
acc_remmetoken CHAR (10) NOT NULL,
acc_remmeip CHAR (15) NOT NULL,
acc_doj DATE NOT NULL,
acc_active INT (1) UNSIGNED NOT NULL,
acc_activatetoken CHAR (10) NOT NULL,
acc_lastvisit INT (11) UNSIGNED NOT NULL,
PRIMARY KEY (id_acc),
UNIQUE (acc_email)
)";

//Chans

$requete_t_chan = "CREATE TABLE t_chan (
id_chan INT (11) UNSIGNED NOT NULL AUTO_INCREMENT,
id_game INT (11) UNSIGNED NOT NULL,
chan_name VARCHAR (32) NOT NULL,
chan_shortname VARCHAR (8) NOT NULL,
PRIMARY KEY (id_chan),
FOREIGN KEY (id_game) REFERENCES t_game(id_game)
)";
 
$requete_t_chanpost = "CREATE TABLE t_chanpost (
id_chanpost INT (11) UNSIGNED NOT NULL AUTO_INCREMENT,
id_chanthread INT (11) UNSIGNED NOT NULL,
id_chan INT (11) UNSIGNED NOT NULL,
id_acc INT (11) UNSIGNED NOT NULL,
chanpost_subject VARCHAR (64) NOT NULL,
chanpost_text TEXT,
chanpost_create INT (11) UNSIGNED NOT NULL,
chanpost_update INT (11) UNSIGNED NOT NULL,
chanpost_url TEXT,
chanpost_ip VARCHAR (15) NOT NULL,
PRIMARY KEY (id_chanpost),
FOREIGN KEY (id_chan) REFERENCES t_chan(id_chan),
FOREIGN KEY (id_acc) REFERENCES t_account(id_acc),
FOREIGN KEY (id_chanthread) REFERENCES t_chanpost(id_chanpost)
)";

?>
