<?php include 'header.php'; ?>
<div id="main">
	<div id="main_wrapper">
		<div id="column_featured">
			<div class="column_container">
				<h2>Featured</h2>
			
				<div class="article">
					<div class="article_header article_purple">Lorem Ipsum</div>
					<div class="article_main">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
					<div class="article_footer article_green"><img src="img/comments.png"/> 0 comments</div>
				</div>
				<div class="smallarticle">
					<div class="smallarticle_header smallarticle_green">Small article header <div class="smalltext"><img src="img/comments.png"/> 2</div></div>
					<div class="smallarticle_main">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
				</div>
				<div class="smallarticle">
					<div class="smallarticle_header smallarticle_purple">Small article header <div class="smalltext"><img src="img/comments.png"/> 2</div></div>
					<div class="smallarticle_main">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
				</div>
				<div class="article">
					<div class="article_header article_purple">Lorem Ipsum</div>
					<div class="article_main">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
					<div class="article_footer article_green"><img src="img/comments.png"/> 0 comments</div>
				</div>
				<div class="smallarticle">
					<div class="smallarticle_header smallarticle_green">Small article header <div class="smalltext"><img src="img/comments.png"/> 2</div></div>
					<div class="smallarticle_main">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
				</div>
				<div class="smallarticle">
					<div class="smallarticle_header smallarticle_purple">Small article header <div class="smalltext"><img src="img/comments.png"/> 2</div></div>
					<div class="smallarticle_main">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
				</div>
				<div class="smallarticle">
					<div class="smallarticle_header smallarticle_green">Small article header <div class="smalltext"><img src="img/comments.png"/> 2</div></div>
					<div class="smallarticle_main">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
				</div>
				<div class="article">
					<div class="article_header article_purple">Lorem Ipsum</div>
					<div class="article_main">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
					<div class="article_footer article_green"><img src="img/comments.png"/> 0 comments</div>
				</div>
			
			
			</div>
		</div>
		<div id="column_user">
			<div class="column_container">
				<h2>User content</h2>
			
				<div class="article">
					<div class="article_header article_green">ARTICLE HEADER</div>
					<div class="article_main">THE ACTUAL ARTICLE</div>
					<div class="article_footer article_purple">ARTICLE FOOTER</div>
				</div>
				<div class="smallarticle">
					<div class="smallarticle_header smallarticle_green">Small article header <div class="smalltext"><img src="img/comments.png"/> 3</div></div>
					<div class="smallarticle_main">Small article text</div>
				</div>
			
			</div>
		</div>
		<div id="column_media">
			<div class="column_container">
				<h2>Medias</h2>
					<div class="article">
						<div class="article_header article_purple">Youtube Video</div>
						<div class="article_main">
							<iframe width="220" height="160" src="//www.youtube.com/embed/vQVeaIHWWck?rel=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="article_footer article_green"><img src="img/comments.png"/> 5 comments</div>
					</div>
			</div>
		</div>
	</div>
<!--
	<div class="box box_green"></div>
	<div class="box box_purple"></div>
	<hr />
	
	
	
	
	
	<div class="article">
		<div class="article_header article_green">ARTICLE HEADER</div>
		<div class="article_main">THE ACTUAL ARTICLE</div>
		<div class="article_footer article_purple">ARTICLE FOOTER</div>
	</div>
	<div class="article">
		<div class="article_header article_purple">ARTICLE HEADER</div>
		<div class="article_main">THE ACTUAL ARTICLE</div>
		<div class="article_footer article_green">ARTICLE FOOTER</div>
	</div>
-->
	<div id="before_footer"></div>
</div>
<?php include 'footer.php'; ?>