<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
		<link rel="stylesheet" href="css.css" />
		<title>Game News Website</title>
    </head>
	<body>
		<?php include 'page_about.php'; ?>
		<div id="page_wrapper">
			<div id="header">
				<img id="vivian" src="img/vivian_playing.png" />
				<h1>THE FIRE RISES</h1>
				<div id="login"><?php include 'login.php'; ?></div>
				<?php include 'menu.php'; ?>
			</div>