<?php
	/*
		File: class.debug.php
		Author: Temet <gamesforvivian@yahoo.com>
		Date: 27.08.2014
		Description:
			A Simple Debug Frame work
	*/


class Debug{
	
	protected $flag = false;
	protected $debuLog;
	protected $debugFileName;
	
	function __construct($doDebug){
		$this->flag = $doDebug;
	}
	
	//Print a string, but only if Debug constructor initialized to true
	function message($aMessage){
		$this->debuLog[] = $aMessage;
		print($aString . "\n");
	}
	
	//Write contents of message buffer over to to file with file 
	//specified by $logFileName
	function writeFile($logFileName){
		
		//We don't use message() becuase if message() calls itself and I 
		//Include this here, an infinite loop is created.
		if($this->flag){
			print("Writing Log out to " . $logFileName . "\n");
		}
		
		$logContents = "LOG FILE: \n";
		$logContents .= "=======================================================\n";
		

		foreach($this->debuLog as $line){
			$logContents .= $line . "\n";
		}
		
		file_put_contents($logFileName, $logContents, FILE_APPEND); 
	}
}

$d = new Debug(true);
$d->message("test message");
$d->message("Another Message");
$d->writeFile("test.log");

?>
